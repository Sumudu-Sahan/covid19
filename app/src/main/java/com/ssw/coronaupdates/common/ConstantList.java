package com.ssw.coronaupdates.common;

public class ConstantList {
    public static final boolean DEV_MODE = true;

    public static final int DEFAULT_CONNECTION_TIMEOUT = 60000;
    public static final int DEFAULT_READ_TIMEOUT = 60000;

    public static final String REQUEST_ERROR_STRING = "error";
}
