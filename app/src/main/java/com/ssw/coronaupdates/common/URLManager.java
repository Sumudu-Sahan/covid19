package com.ssw.coronaupdates.common;

public class URLManager {
    private static final String BASE_URL = "https://www.healthpromo.gov.lk";

    private static final String URL_GET_API = "api";
    public static final String URL_GET_CURRENT_STAT = BASE_URL + "/" + URL_GET_API + "/" + "get-current-statistical";
}
