package com.ssw.coronaupdates.communication;

import android.content.Context;
import android.os.AsyncTask;

import com.ssw.coronaupdates.common.ConstantList;
import com.ssw.coronaupdates.util.ExceptionManager;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

public class CommunicationManager extends AsyncTask<String, Void, String> {

    private String content;
    private String error = null;
    private WeakReference<Context> weakReference;
    private RequestCompletionListener requestCompletionListener;

    //TXN Type
    private String txnType = "-1";

    private String[] urls;

    public interface RequestCompletionListener {
        void onRequestSuccess(String response);

        void onRequestFailed(String error);
    }

    public CommunicationManager(Context context, RequestCompletionListener requestCompletionListener) {
        this.weakReference = new WeakReference<>(context);
        this.requestCompletionListener = requestCompletionListener;
    }

    protected void onPreExecute() {
        super.onPreExecute();
    }

    protected String doInBackground(String... urls) {
        HttpURLConnection urlConnection = null;

        this.urls = urls;

        try {
            URL urlToRequest = new URL(urls[0]);
            urlConnection = (HttpURLConnection) urlToRequest.openConnection();

            String body = urls[2];

            if (ConstantList.DEV_MODE) {
                System.err.println("REQUEST URL: " + urlToRequest.toString());
                System.err.println("REQUEST BODY: " + body);
            }

            urlConnection.setDoInput(true);
            urlConnection.setRequestMethod(urls[1]);
            urlConnection.setRequestProperty("Content-Type", "text/plain");
            urlConnection.setRequestProperty("Accept", "text/plain");
            urlConnection.setConnectTimeout(ConstantList.DEFAULT_CONNECTION_TIMEOUT);
            urlConnection.setReadTimeout(ConstantList.DEFAULT_READ_TIMEOUT);

            urlConnection.connect();

//            if (body.length() > 0) {
//                JSONObject jsonParam = new JSONObject(body);
//                DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
//                wr.writeBytes(jsonParam.toString());
//
//                wr.flush();
//                wr.close();
//            }

            if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                error = ConstantList.REQUEST_ERROR_STRING;
                return error;
            }

            //Read
            BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));

            String line;
            StringBuilder sb = new StringBuilder();

            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();

            content = sb.toString();

            return content;
        } catch (Exception e) {
            ExceptionManager.exceptionParser(e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return error;
    }

    protected void onPostExecute(String response) {
        super.onPostExecute(response);
        try {
            if (ConstantList.DEV_MODE) {
                System.err.println("RESPONSE: " + response);
            }
            if (response != null && !response.isEmpty()) {
                if (response.equalsIgnoreCase(ConstantList.REQUEST_ERROR_STRING)) {
                    requestCompletionListener.onRequestFailed(response);
                } else {
                    requestCompletionListener.onRequestSuccess(response);
                }
            }
        } catch (Exception ex) {
            ExceptionManager.exceptionParser(ex);
            requestCompletionListener.onRequestSuccess(ex.toString());
        }
    }
}
