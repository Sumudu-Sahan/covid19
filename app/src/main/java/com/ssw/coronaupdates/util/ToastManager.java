package com.ssw.coronaupdates.util;

import android.content.Context;
import android.widget.Toast;

public class ToastManager {

    private static ToastManager toastManager;

    public static synchronized ToastManager getInstance() {
        if (toastManager == null) {
            toastManager = new ToastManager();
        }
        return toastManager;
    }

    public void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
