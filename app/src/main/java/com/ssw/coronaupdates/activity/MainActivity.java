package com.ssw.coronaupdates.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.ssw.coronaupdates.R;
import com.ssw.coronaupdates.adapter.HospitalListAdapter;
import com.ssw.coronaupdates.controller.CommunicationController;
import com.ssw.coronaupdates.dto.BeanCorona;
import com.ssw.coronaupdates.util.ExceptionManager;
import com.ssw.coronaupdates.util.ToastManager;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity {

    private TextView tvLastUpdatedTime;
    private TextView tvLocalNewCases;
    private TextView tvLocalTotalCases;
    private TextView tvLocalTotalSuspections;
    private TextView tvLocalNewDeaths;
    private TextView tvLocalTotalDeaths;
    private TextView tvLocalRecovered;

    private TextView tvGlobalNewCases;
    private TextView tvGlobalTotalCases;
    private TextView tvGlobalNewDeaths;
    private TextView tvGlobalTotalDeaths;
    private TextView tvGlobalRecovered;

    private RecyclerView rvHospitalList;

    private HospitalListAdapter hospitalListAdapter;
    private List<BeanCorona.DataBean.HospitalDataBean> hospitalDataBeans = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initObjects();
    }

    private void initObjects() {
        initComponents();
    }

    private void initComponents() {
        tvLastUpdatedTime = findViewById(R.id.tvLastUpdatedTime);
        tvLocalNewCases = findViewById(R.id.tvLocalNewCases);
        tvLocalTotalCases = findViewById(R.id.tvLocalTotalCases);
        tvLocalTotalSuspections = findViewById(R.id.tvLocalTotalSuspections);
        tvLocalNewDeaths = findViewById(R.id.tvLocalNewDeaths);
        tvLocalTotalDeaths = findViewById(R.id.tvLocalTotalDeaths);
        tvLocalRecovered = findViewById(R.id.tvLocalRecovered);

        tvGlobalNewCases = findViewById(R.id.tvGlobalNewCases);
        tvGlobalTotalCases = findViewById(R.id.tvGlobalTotalCases);
        tvGlobalNewDeaths = findViewById(R.id.tvGlobalNewDeaths);
        tvGlobalTotalDeaths = findViewById(R.id.tvGlobalTotalDeaths);
        tvGlobalRecovered = findViewById(R.id.tvGlobalRecovered);

        rvHospitalList = findViewById(R.id.rvHospitalList);

        initAdapter();
    }

    private void initAdapter() {
        hospitalListAdapter = new HospitalListAdapter(new HospitalListAdapter.HospitalItemClickListener() {
            @Override
            public void onItemClicked(BeanCorona.DataBean.HospitalDataBean hospitalDataBean) {

            }

            @Override
            public boolean onItemLongClicked(BeanCorona.DataBean.HospitalDataBean hospitalDataBean) {
                return false;
            }
        }, hospitalDataBeans);

        rvHospitalList.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        rvHospitalList.setItemAnimator(new DefaultItemAnimator());
        rvHospitalList.setAdapter(hospitalListAdapter);

        loadData(MainActivity.this);
    }

    private void loadData(final Context context) {
        final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please Wait...");
        CommunicationController.getInstance().getLatestUpdates(context, new CommunicationController.CommunicationCompletionListener() {
            @Override
            public void onRequestSuccess(String response) {
                BeanCorona beanCorona = new Gson().fromJson(response, BeanCorona.class);
                try {
                    progressDialog.dismiss();
                } catch (Exception e) {
                    ExceptionManager.exceptionParser(e);
                }
                if (beanCorona.isSuccess()) {
                    hospitalDataBeans.clear();
                    hospitalDataBeans.addAll(beanCorona.getData().getHospital_data());
                    hospitalListAdapter.notifyDataSetChanged();

                    tvLastUpdatedTime.setText("Last Updated Time : " + beanCorona.getData().getUpdate_date_time());
                    tvLocalNewCases.setText("Local New Cases : " + beanCorona.getData().getLocal_new_cases());
                    tvLocalTotalCases.setText("Local Total Cases : " + beanCorona.getData().getLocal_total_cases());
                    tvLocalTotalSuspections.setText("Local Total Suspects : " + beanCorona.getData().getLocal_total_number_of_individuals_in_hospitals());
                    tvLocalNewDeaths.setText("Local New Deaths : " + beanCorona.getData().getLocal_new_deaths());
                    tvLocalTotalDeaths.setText("Local Total Deaths : " + beanCorona.getData().getLocal_deaths());
                    tvLocalRecovered.setText("Local Total Recovered Count : " + beanCorona.getData().getLocal_recovered());

                    tvGlobalNewCases.setText("Global New Cases : " + beanCorona.getData().getGlobal_new_cases());
                    tvGlobalTotalCases.setText("Global Total Cases : " + beanCorona.getData().getGlobal_total_cases());
                    tvGlobalNewDeaths.setText("Global New Deaths : " + beanCorona.getData().getGlobal_new_deaths());
                    tvGlobalTotalDeaths.setText("Global Total Deaths : " + beanCorona.getData().getGlobal_deaths());
                    tvGlobalRecovered.setText("Global Total Recovered Count : " + beanCorona.getData().getGlobal_recovered());
                } else {
                    ToastManager.getInstance().showToast(context, beanCorona.getMessage());
                }
            }

            @Override
            public void onRequestFailed(String error) {
                try {
                    progressDialog.dismiss();
                } catch (Exception e) {
                    ExceptionManager.exceptionParser(e);
                }
                ToastManager.getInstance().showToast(context, error);
            }
        });
    }
}
