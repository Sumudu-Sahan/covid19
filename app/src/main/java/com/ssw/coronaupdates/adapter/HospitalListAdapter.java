package com.ssw.coronaupdates.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ssw.coronaupdates.R;
import com.ssw.coronaupdates.dto.BeanCorona;

import java.util.ArrayList;
import java.util.List;

public class HospitalListAdapter extends RecyclerView.Adapter<HospitalListAdapter.MasonryView> implements Filterable {
    private List<BeanCorona.DataBean.HospitalDataBean> hospitalDataBeans;
    private List<BeanCorona.DataBean.HospitalDataBean> filteredHospitalDataBeans;

    private ValueFilter valueFilter;

    private HospitalItemClickListener hospitalItemClickListener;

    public HospitalListAdapter(HospitalItemClickListener hospitalItemClickListener, List<BeanCorona.DataBean.HospitalDataBean> hospitalDataBeans) {
        this.hospitalItemClickListener = hospitalItemClickListener;

        this.hospitalDataBeans = hospitalDataBeans;
        this.filteredHospitalDataBeans = hospitalDataBeans;

        getFilter();
    }

    public interface HospitalItemClickListener {
        void onItemClicked(BeanCorona.DataBean.HospitalDataBean hospitalDataBean);

        boolean onItemLongClicked(BeanCorona.DataBean.HospitalDataBean hospitalDataBean);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return hospitalDataBeans.size();
    }

    @Override
    public MasonryView onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_hospital, parent, false);
        return new MasonryView(layoutView);
    }

    @Override
    public void onBindViewHolder(final MasonryView holder, final int position) {
        final BeanCorona.DataBean.HospitalDataBean hospitalDataBean = hospitalDataBeans.get(position);

        holder.tvHospitalNameEN.setText(hospitalDataBean.getHospital().getName());
        holder.tvHospitalNameSI.setText(hospitalDataBean.getHospital().getName_si());
        holder.tvHospitalNameTA.setText(hospitalDataBean.getHospital().getName_ta());

        holder.tvCumulativeLocal.setText("Cumulative Local : " + hospitalDataBean.getCumulative_local());
        holder.tvCumulativeForeign.setText("Cumulative Foreign : " + hospitalDataBean.getCumulative_foreign());
        holder.tvCumulativeTotal.setText("Cumulative Total : " + hospitalDataBean.getCumulative_total());

        holder.tvTreatmentLocal.setText("Treatment Local : " + hospitalDataBean.getTreatment_local());
        holder.tvTreatmentForeign.setText("Treatment Foreign : " + hospitalDataBean.getTreatment_foreign());
        holder.tvTreatmentTotal.setText("Treatment Total : " + hospitalDataBean.getTreatment_total());

        holder.cvHospitalItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hospitalItemClickListener.onItemClicked(hospitalDataBean);
            }
        });

        holder.cvHospitalItem.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return hospitalItemClickListener.onItemLongClicked(hospitalDataBean);
            }
        });
    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    class MasonryView extends RecyclerView.ViewHolder {
        TextView tvHospitalNameEN;
        TextView tvHospitalNameSI;
        TextView tvHospitalNameTA;

        TextView tvCumulativeLocal;
        TextView tvCumulativeForeign;
        TextView tvCumulativeTotal;

        TextView tvTreatmentLocal;
        TextView tvTreatmentForeign;
        TextView tvTreatmentTotal;

        CardView cvHospitalItem;

        private MasonryView(View itemView) {
            super(itemView);
            cvHospitalItem = itemView.findViewById(R.id.cvHospitalItem);

            tvHospitalNameEN = itemView.findViewById(R.id.tvHospitalNameEN);
            tvHospitalNameSI = itemView.findViewById(R.id.tvHospitalNameSI);
            tvHospitalNameTA = itemView.findViewById(R.id.tvHospitalNameTA);

            tvCumulativeLocal = itemView.findViewById(R.id.tvCumulativeLocal);
            tvCumulativeForeign = itemView.findViewById(R.id.tvCumulativeForeign);
            tvCumulativeTotal = itemView.findViewById(R.id.tvCumulativeTotal);

            tvTreatmentLocal = itemView.findViewById(R.id.tvTreatmentLocal);
            tvTreatmentForeign = itemView.findViewById(R.id.tvTreatmentForeign);
            tvTreatmentTotal = itemView.findViewById(R.id.tvTreatmentTotal);
        }
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                List<BeanCorona.DataBean.HospitalDataBean> filterList = new ArrayList<>();

                for (int i = 0; i < filteredHospitalDataBeans.size(); i++) {
                    if ((filteredHospitalDataBeans.get(i).getHospital().getName().toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(filteredHospitalDataBeans.get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = filteredHospitalDataBeans.size();
                results.values = filteredHospitalDataBeans;
            }
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            hospitalDataBeans = (ArrayList<BeanCorona.DataBean.HospitalDataBean>) results.values;
            notifyDataSetChanged();
        }
    }
}