package com.ssw.coronaupdates.controller;

import android.content.Context;

import com.google.gson.JsonObject;
import com.ssw.coronaupdates.common.URLManager;
import com.ssw.coronaupdates.communication.CommunicationManager;
import com.ssw.coronaupdates.util.ExceptionManager;
import com.ssw.coronaupdates.util.MyDevice;

public class CommunicationController {
    private static CommunicationController communicationController;

    private final String METHOD_GET = "GET";
    private final String METHOD_POST = "POST";

    public static synchronized CommunicationController getInstance() {
        if (communicationController == null) {
            communicationController = new CommunicationController();
        }
        return communicationController;
    }

    public interface CommunicationCompletionListener {
        public void onRequestSuccess(String response);

        public void onRequestFailed(String error);
    }

    public void getLatestUpdates(Context context, final CommunicationCompletionListener communicationCompletionListener) {
        try {
            if (MyDevice.isInternetAvailable(context)) {
                new CommunicationManager(context, new CommunicationManager.RequestCompletionListener() {
                    @Override
                    public void onRequestSuccess(String response) {
                        communicationCompletionListener.onRequestSuccess(response);
                    }

                    @Override
                    public void onRequestFailed(String error) {
                        communicationCompletionListener.onRequestFailed(error);
                    }
                }).execute(URLManager.URL_GET_CURRENT_STAT, METHOD_GET, new JsonObject().toString());
            }
        } catch (Exception e) {
            ExceptionManager.exceptionParser(e);
        }
    }
}
