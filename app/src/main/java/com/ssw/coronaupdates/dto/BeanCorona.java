package com.ssw.coronaupdates.dto;

import java.io.Serializable;
import java.util.List;

public class BeanCorona implements Serializable {

    public static final long serialVersionUID = 12345678904354321L;

    /**
     * success : true
     * message : Success
     * data : {"update_date_time":"2020-03-21 18:57:17","local_new_cases":5,"local_total_cases":77,"local_total_number_of_individuals_in_hospitals":245,"local_deaths":0,"local_new_deaths":0,"local_recovered":1,"global_new_cases":30665,"global_total_cases":283256,"global_deaths":11829,"global_new_deaths":1061,"global_recovered":91573,"hospital_data":[{"id":1,"hospital_id":1,"cumulative_local":459,"cumulative_foreign":76,"treatment_local":84,"treatment_foreign":2,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":535,"treatment_total":86,"hospital":{"id":1,"name":"National Institute of Infectious Diseases","name_si":"බෝවන රෝග පිළිබඳ ජාතික ආයතනය","name_ta":"தேசிய தொற்று நோயியல் நிறுவனம்","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":2,"hospital_id":2,"cumulative_local":59,"cumulative_foreign":4,"treatment_local":13,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":63,"treatment_total":13,"hospital":{"id":2,"name":"National Hospital Sri Lanka","name_si":"ශ්\u200dරී ලංකා ජාතික රෝහල","name_ta":"இலங்கை தேசிய வைத்தியசாலை","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":3,"hospital_id":3,"cumulative_local":51,"cumulative_foreign":2,"treatment_local":16,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":53,"treatment_total":16,"hospital":{"id":3,"name":"TH - Ragama","name_si":"ශික්ෂණ රෝහල - රාගම","name_ta":"போதனா வைத்தியசாலை - ராகம","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":4,"hospital_id":4,"cumulative_local":82,"cumulative_foreign":12,"treatment_local":4,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":94,"treatment_total":4,"hospital":{"id":4,"name":"TH - Karapitiya","name_si":"ශික්ෂණ රෝහල - කරපිටිය","name_ta":"போதனா வைத்தியசாலை - கராபிடிய","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":5,"hospital_id":5,"cumulative_local":34,"cumulative_foreign":2,"treatment_local":10,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":36,"treatment_total":10,"hospital":{"id":5,"name":"TH - Anuradhapura","name_si":"ශික්ෂණ රෝහල - අනුරාධපුර","name_ta":"போதனா வைத்தியசாலை - அனுராதபுரம்","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":6,"hospital_id":6,"cumulative_local":91,"cumulative_foreign":1,"treatment_local":7,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":92,"treatment_total":7,"hospital":{"id":6,"name":"TH - Kurunegala","name_si":"ශික්ෂණ රෝහල - කුරුණගල","name_ta":"போதனா வைத்தியசாலை - குருநாகல்","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":7,"hospital_id":7,"cumulative_local":10,"cumulative_foreign":1,"treatment_local":1,"treatment_foreign":1,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":11,"treatment_total":2,"hospital":{"id":7,"name":"TH- Jaffna","name_si":"ශික්ෂණ රෝහල - යාපනය","name_ta":"போதனா வைத்தியசாலை - யாழ்ப்பாணம்","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":8,"hospital_id":8,"cumulative_local":47,"cumulative_foreign":5,"treatment_local":4,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":52,"treatment_total":4,"hospital":{"id":8,"name":"National Hospital Kandy","name_si":"ජාතික රෝහල - මහනුවර","name_ta":"தேசிய வைத்தியசாலை கண்டி","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":9,"hospital_id":9,"cumulative_local":10,"cumulative_foreign":0,"treatment_local":2,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":10,"treatment_total":2,"hospital":{"id":9,"name":"TH \u2013 Batticaloa","name_si":"ශික්ෂණ රෝහල - මඩකලපුව","name_ta":"போதனா வைத்தியசாலை - மட்டக்களப்பு","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":10,"hospital_id":10,"cumulative_local":65,"cumulative_foreign":4,"treatment_local":23,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":69,"treatment_total":23,"hospital":{"id":10,"name":"DGH- Gampaha","name_si":"දිස්ත්\u200dරික් මහ රෝහල - ගම්පහ","name_ta":"மாவட்ட பொது வைத்தியசாலை - கம்பஹா","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":11,"hospital_id":11,"cumulative_local":81,"cumulative_foreign":12,"treatment_local":16,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":93,"treatment_total":16,"hospital":{"id":11,"name":"DGH \u2013 Negombo","name_si":"දිස්ත්\u200dරික් මහ රෝහල - මීගමුව","name_ta":"மாவட்ட பொது வைத்தியசாலை - நீர் கொழும்பு","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":12,"hospital_id":12,"cumulative_local":62,"cumulative_foreign":0,"treatment_local":9,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":62,"treatment_total":9,"hospital":{"id":12,"name":"TH \u2013 Rathnapura","name_si":"ශික්ෂණ රෝහල - රත්නපුර","name_ta":"போதனா வைத்தியசாலை - இரத்தினபுரி","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":13,"hospital_id":13,"cumulative_local":21,"cumulative_foreign":0,"treatment_local":6,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":21,"treatment_total":6,"hospital":{"id":13,"name":"PGH \u2013 Badulla","name_si":"පළාත් මහ රෝහල - බදුල්ල","name_ta":"மாகான அரச வைத்தியசாலை - பதுளை","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":14,"hospital_id":14,"cumulative_local":21,"cumulative_foreign":0,"treatment_local":0,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":21,"treatment_total":0,"hospital":{"id":14,"name":"LRH","name_si":"රිජ්වේ ආර්යා ළමා රෝහල","name_ta":"லேடி ரிட்ஜ்வே வைத்தியசாலை","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":15,"hospital_id":15,"cumulative_local":2,"cumulative_foreign":0,"treatment_local":0,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":2,"treatment_total":0,"hospital":{"id":15,"name":"DMH","name_si":"ඩි සොයිසා කාන්තා රෝහල","name_ta":"டி சோய்சா மகப்பேறு வைத்தியசாலை","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":16,"hospital_id":16,"cumulative_local":46,"cumulative_foreign":5,"treatment_local":16,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":51,"treatment_total":16,"hospital":{"id":16,"name":"DGH \u2013 Polonnaruwa","name_si":"දිස්ත්\u200dරික් මහ රෝහල - පොලොන්නරුව","name_ta":"மாவட்ட பொது வைத்தியசாலை - பொலன்னறுவை","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":17,"hospital_id":17,"cumulative_local":16,"cumulative_foreign":0,"treatment_local":0,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":16,"treatment_total":0,"hospital":{"id":17,"name":"TH - Kalubowila","name_si":"දිස්ත්\u200dරික් මහ රෝහල - කලුබෝවිල","name_ta":"கொழும்பு தெற்கு போதனா வைத்தியசாலை","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":18,"hospital_id":18,"cumulative_local":2,"cumulative_foreign":0,"treatment_local":1,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":2,"treatment_total":1,"hospital":{"id":18,"name":"Castle Street Teaching Hospital","name_si":"කාසල් වීදියේ ශික්ෂණ රෝහල","name_ta":"காசல் வீதி மகளிர் போதனா மருத்துவமனை","created_at":"2020-03-21 00:00:00","updated_at":"2020-03-21 00:00:00","deleted_at":null}}]}
     */

    private boolean success;
    private String message;
    private DataBean data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {

        public static final long serialVersionUID = 12345678904354321L;

        /**
         * update_date_time : 2020-03-21 18:57:17
         * local_new_cases : 5
         * local_total_cases : 77
         * local_total_number_of_individuals_in_hospitals : 245
         * local_deaths : 0
         * local_new_deaths : 0
         * local_recovered : 1
         * global_new_cases : 30665
         * global_total_cases : 283256
         * global_deaths : 11829
         * global_new_deaths : 1061
         * global_recovered : 91573
         * hospital_data : [{"id":1,"hospital_id":1,"cumulative_local":459,"cumulative_foreign":76,"treatment_local":84,"treatment_foreign":2,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":535,"treatment_total":86,"hospital":{"id":1,"name":"National Institute of Infectious Diseases","name_si":"බෝවන රෝග පිළිබඳ ජාතික ආයතනය","name_ta":"தேசிய தொற்று நோயியல் நிறுவனம்","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":2,"hospital_id":2,"cumulative_local":59,"cumulative_foreign":4,"treatment_local":13,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":63,"treatment_total":13,"hospital":{"id":2,"name":"National Hospital Sri Lanka","name_si":"ශ්\u200dරී ලංකා ජාතික රෝහල","name_ta":"இலங்கை தேசிய வைத்தியசாலை","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":3,"hospital_id":3,"cumulative_local":51,"cumulative_foreign":2,"treatment_local":16,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":53,"treatment_total":16,"hospital":{"id":3,"name":"TH - Ragama","name_si":"ශික්ෂණ රෝහල - රාගම","name_ta":"போதனா வைத்தியசாலை - ராகம","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":4,"hospital_id":4,"cumulative_local":82,"cumulative_foreign":12,"treatment_local":4,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":94,"treatment_total":4,"hospital":{"id":4,"name":"TH - Karapitiya","name_si":"ශික්ෂණ රෝහල - කරපිටිය","name_ta":"போதனா வைத்தியசாலை - கராபிடிய","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":5,"hospital_id":5,"cumulative_local":34,"cumulative_foreign":2,"treatment_local":10,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":36,"treatment_total":10,"hospital":{"id":5,"name":"TH - Anuradhapura","name_si":"ශික්ෂණ රෝහල - අනුරාධපුර","name_ta":"போதனா வைத்தியசாலை - அனுராதபுரம்","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":6,"hospital_id":6,"cumulative_local":91,"cumulative_foreign":1,"treatment_local":7,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":92,"treatment_total":7,"hospital":{"id":6,"name":"TH - Kurunegala","name_si":"ශික්ෂණ රෝහල - කුරුණගල","name_ta":"போதனா வைத்தியசாலை - குருநாகல்","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":7,"hospital_id":7,"cumulative_local":10,"cumulative_foreign":1,"treatment_local":1,"treatment_foreign":1,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":11,"treatment_total":2,"hospital":{"id":7,"name":"TH- Jaffna","name_si":"ශික්ෂණ රෝහල - යාපනය","name_ta":"போதனா வைத்தியசாலை - யாழ்ப்பாணம்","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":8,"hospital_id":8,"cumulative_local":47,"cumulative_foreign":5,"treatment_local":4,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":52,"treatment_total":4,"hospital":{"id":8,"name":"National Hospital Kandy","name_si":"ජාතික රෝහල - මහනුවර","name_ta":"தேசிய வைத்தியசாலை கண்டி","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":9,"hospital_id":9,"cumulative_local":10,"cumulative_foreign":0,"treatment_local":2,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":10,"treatment_total":2,"hospital":{"id":9,"name":"TH \u2013 Batticaloa","name_si":"ශික්ෂණ රෝහල - මඩකලපුව","name_ta":"போதனா வைத்தியசாலை - மட்டக்களப்பு","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":10,"hospital_id":10,"cumulative_local":65,"cumulative_foreign":4,"treatment_local":23,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":69,"treatment_total":23,"hospital":{"id":10,"name":"DGH- Gampaha","name_si":"දිස්ත්\u200dරික් මහ රෝහල - ගම්පහ","name_ta":"மாவட்ட பொது வைத்தியசாலை - கம்பஹா","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":11,"hospital_id":11,"cumulative_local":81,"cumulative_foreign":12,"treatment_local":16,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":93,"treatment_total":16,"hospital":{"id":11,"name":"DGH \u2013 Negombo","name_si":"දිස්ත්\u200dරික් මහ රෝහල - මීගමුව","name_ta":"மாவட்ட பொது வைத்தியசாலை - நீர் கொழும்பு","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":12,"hospital_id":12,"cumulative_local":62,"cumulative_foreign":0,"treatment_local":9,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":62,"treatment_total":9,"hospital":{"id":12,"name":"TH \u2013 Rathnapura","name_si":"ශික්ෂණ රෝහල - රත්නපුර","name_ta":"போதனா வைத்தியசாலை - இரத்தினபுரி","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":13,"hospital_id":13,"cumulative_local":21,"cumulative_foreign":0,"treatment_local":6,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":21,"treatment_total":6,"hospital":{"id":13,"name":"PGH \u2013 Badulla","name_si":"පළාත් මහ රෝහල - බදුල්ල","name_ta":"மாகான அரச வைத்தியசாலை - பதுளை","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":14,"hospital_id":14,"cumulative_local":21,"cumulative_foreign":0,"treatment_local":0,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":21,"treatment_total":0,"hospital":{"id":14,"name":"LRH","name_si":"රිජ්වේ ආර්යා ළමා රෝහල","name_ta":"லேடி ரிட்ஜ்வே வைத்தியசாலை","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":15,"hospital_id":15,"cumulative_local":2,"cumulative_foreign":0,"treatment_local":0,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":2,"treatment_total":0,"hospital":{"id":15,"name":"DMH","name_si":"ඩි සොයිසා කාන්තා රෝහල","name_ta":"டி சோய்சா மகப்பேறு வைத்தியசாலை","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":16,"hospital_id":16,"cumulative_local":46,"cumulative_foreign":5,"treatment_local":16,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":51,"treatment_total":16,"hospital":{"id":16,"name":"DGH \u2013 Polonnaruwa","name_si":"දිස්ත්\u200dරික් මහ රෝහල - පොලොන්නරුව","name_ta":"மாவட்ட பொது வைத்தியசாலை - பொலன்னறுவை","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":17,"hospital_id":17,"cumulative_local":16,"cumulative_foreign":0,"treatment_local":0,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":16,"treatment_total":0,"hospital":{"id":17,"name":"TH - Kalubowila","name_si":"දිස්ත්\u200dරික් මහ රෝහල - කලුබෝවිල","name_ta":"கொழும்பு தெற்கு போதனா வைத்தியசாலை","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}},{"id":18,"hospital_id":18,"cumulative_local":2,"cumulative_foreign":0,"treatment_local":1,"treatment_foreign":0,"created_at":"2020-03-21 12:26:14","updated_at":null,"deleted_at":null,"cumulative_total":2,"treatment_total":1,"hospital":{"id":18,"name":"Castle Street Teaching Hospital","name_si":"කාසල් වීදියේ ශික්ෂණ රෝහල","name_ta":"காசல் வீதி மகளிர் போதனா மருத்துவமனை","created_at":"2020-03-21 00:00:00","updated_at":"2020-03-21 00:00:00","deleted_at":null}}]
         */

        private String update_date_time;
        private int local_new_cases;
        private int local_total_cases;
        private int local_total_number_of_individuals_in_hospitals;
        private int local_deaths;
        private int local_new_deaths;
        private int local_recovered;
        private int global_new_cases;
        private int global_total_cases;
        private int global_deaths;
        private int global_new_deaths;
        private int global_recovered;
        private List<HospitalDataBean> hospital_data;

        public String getUpdate_date_time() {
            return update_date_time;
        }

        public void setUpdate_date_time(String update_date_time) {
            this.update_date_time = update_date_time;
        }

        public int getLocal_new_cases() {
            return local_new_cases;
        }

        public void setLocal_new_cases(int local_new_cases) {
            this.local_new_cases = local_new_cases;
        }

        public int getLocal_total_cases() {
            return local_total_cases;
        }

        public void setLocal_total_cases(int local_total_cases) {
            this.local_total_cases = local_total_cases;
        }

        public int getLocal_total_number_of_individuals_in_hospitals() {
            return local_total_number_of_individuals_in_hospitals;
        }

        public void setLocal_total_number_of_individuals_in_hospitals(int local_total_number_of_individuals_in_hospitals) {
            this.local_total_number_of_individuals_in_hospitals = local_total_number_of_individuals_in_hospitals;
        }

        public int getLocal_deaths() {
            return local_deaths;
        }

        public void setLocal_deaths(int local_deaths) {
            this.local_deaths = local_deaths;
        }

        public int getLocal_new_deaths() {
            return local_new_deaths;
        }

        public void setLocal_new_deaths(int local_new_deaths) {
            this.local_new_deaths = local_new_deaths;
        }

        public int getLocal_recovered() {
            return local_recovered;
        }

        public void setLocal_recovered(int local_recovered) {
            this.local_recovered = local_recovered;
        }

        public int getGlobal_new_cases() {
            return global_new_cases;
        }

        public void setGlobal_new_cases(int global_new_cases) {
            this.global_new_cases = global_new_cases;
        }

        public int getGlobal_total_cases() {
            return global_total_cases;
        }

        public void setGlobal_total_cases(int global_total_cases) {
            this.global_total_cases = global_total_cases;
        }

        public int getGlobal_deaths() {
            return global_deaths;
        }

        public void setGlobal_deaths(int global_deaths) {
            this.global_deaths = global_deaths;
        }

        public int getGlobal_new_deaths() {
            return global_new_deaths;
        }

        public void setGlobal_new_deaths(int global_new_deaths) {
            this.global_new_deaths = global_new_deaths;
        }

        public int getGlobal_recovered() {
            return global_recovered;
        }

        public void setGlobal_recovered(int global_recovered) {
            this.global_recovered = global_recovered;
        }

        public List<HospitalDataBean> getHospital_data() {
            return hospital_data;
        }

        public void setHospital_data(List<HospitalDataBean> hospital_data) {
            this.hospital_data = hospital_data;
        }

        public static class HospitalDataBean {
            /**
             * id : 1
             * hospital_id : 1
             * cumulative_local : 459
             * cumulative_foreign : 76
             * treatment_local : 84
             * treatment_foreign : 2
             * created_at : 2020-03-21 12:26:14
             * updated_at : null
             * deleted_at : null
             * cumulative_total : 535
             * treatment_total : 86
             * hospital : {"id":1,"name":"National Institute of Infectious Diseases","name_si":"බෝවන රෝග පිළිබඳ ජාතික ආයතනය","name_ta":"தேசிய தொற்று நோயியல் நிறுவனம்","created_at":"2020-03-20 01:54:12","updated_at":"2020-03-20 01:54:12","deleted_at":null}
             */

            private int id;
            private int hospital_id;
            private int cumulative_local;
            private int cumulative_foreign;
            private int treatment_local;
            private int treatment_foreign;
            private String created_at;
            private Object updated_at;
            private Object deleted_at;
            private int cumulative_total;
            private int treatment_total;
            private HospitalBean hospital;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getHospital_id() {
                return hospital_id;
            }

            public void setHospital_id(int hospital_id) {
                this.hospital_id = hospital_id;
            }

            public int getCumulative_local() {
                return cumulative_local;
            }

            public void setCumulative_local(int cumulative_local) {
                this.cumulative_local = cumulative_local;
            }

            public int getCumulative_foreign() {
                return cumulative_foreign;
            }

            public void setCumulative_foreign(int cumulative_foreign) {
                this.cumulative_foreign = cumulative_foreign;
            }

            public int getTreatment_local() {
                return treatment_local;
            }

            public void setTreatment_local(int treatment_local) {
                this.treatment_local = treatment_local;
            }

            public int getTreatment_foreign() {
                return treatment_foreign;
            }

            public void setTreatment_foreign(int treatment_foreign) {
                this.treatment_foreign = treatment_foreign;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public Object getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(Object updated_at) {
                this.updated_at = updated_at;
            }

            public Object getDeleted_at() {
                return deleted_at;
            }

            public void setDeleted_at(Object deleted_at) {
                this.deleted_at = deleted_at;
            }

            public int getCumulative_total() {
                return cumulative_total;
            }

            public void setCumulative_total(int cumulative_total) {
                this.cumulative_total = cumulative_total;
            }

            public int getTreatment_total() {
                return treatment_total;
            }

            public void setTreatment_total(int treatment_total) {
                this.treatment_total = treatment_total;
            }

            public HospitalBean getHospital() {
                return hospital;
            }

            public void setHospital(HospitalBean hospital) {
                this.hospital = hospital;
            }

            public static class HospitalBean {
                /**
                 * id : 1
                 * name : National Institute of Infectious Diseases
                 * name_si : බෝවන රෝග පිළිබඳ ජාතික ආයතනය
                 * name_ta : தேசிய தொற்று நோயியல் நிறுவனம்
                 * created_at : 2020-03-20 01:54:12
                 * updated_at : 2020-03-20 01:54:12
                 * deleted_at : null
                 */

                private int id;
                private String name;
                private String name_si;
                private String name_ta;
                private String created_at;
                private String updated_at;
                private Object deleted_at;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getName_si() {
                    return name_si;
                }

                public void setName_si(String name_si) {
                    this.name_si = name_si;
                }

                public String getName_ta() {
                    return name_ta;
                }

                public void setName_ta(String name_ta) {
                    this.name_ta = name_ta;
                }

                public String getCreated_at() {
                    return created_at;
                }

                public void setCreated_at(String created_at) {
                    this.created_at = created_at;
                }

                public String getUpdated_at() {
                    return updated_at;
                }

                public void setUpdated_at(String updated_at) {
                    this.updated_at = updated_at;
                }

                public Object getDeleted_at() {
                    return deleted_at;
                }

                public void setDeleted_at(Object deleted_at) {
                    this.deleted_at = deleted_at;
                }
            }
        }
    }
}
